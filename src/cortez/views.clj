(ns cortez.views
  [:use hiccup.page])

(defn head []
  [:head
   [:link {:rel "stylesheet"
           :type "text/css"
           :href "/static/main.css"}]])

(defn header []
  [:header
   [:h1 "Cortez"]
   [:nav
    [:a {:href "/"} "Home"]
    [:a {:href "/login"} "Log in"]]])

(defn page
  ([page-fn]
   (html5
    {:lang "en"}
    (head)
    (into  [:body
            (header)]
           (page-fn))))
  ([page-fn model]
   (html5
    {:lang "en"}
    (head)
    (into [:body
           (header)]
          (page-fn model)))))

(defn home []
  [[:h2 "Welcome to Cortez!"]
   [:p "Cortez is "
    [:a
     {:href "https://gitlab.com/pjsmith"}
     "Peter Smith's"]
    " simple Clojure blog project. It's a toy project for "
    "learning about the language and ecosystem"]
   [:p "Interesting topics include:"]
   [:ul
    [:li "Ring and the Clojure web ecosystem"]
    [:li "Macros and the tasteful use of macros"]
    [:li "Use of <code>clojure.spec</code>"]
    [:li "ClojureScript and full-stack Clojure"]]])

(defn login []
  [[:h2 "Log in to edit"]
   [:form {:method "POST" :action "/login"}
    [:label {:for "username"} "Username"]
    [:input {:type "text" :name "username" :placeholder "Username"}]
    [:label {:for "password"} "Password"]
    [:input {:type "password" :name "password" :placeholder "Password"}]
    [:button {:type "submit"} "Log in"]]])
