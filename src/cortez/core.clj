(ns cortez.core
  (:require [cortez.views :as v])
  (:require [cortez.auth :as auth])
  (:require [clojure.string :as s])
  (:require [clojure.java.io :as j.io]))

(defn error-trap [handler]
  (fn [request]
    (try (handler request)
         (catch Exception ex {:status 500
                              :headers {"Content-Type" "text/html"}
                              :body (str "<h1>Woah, sorry!</h1>" "<p>" ex "</p>")}))))

(defn split-kvps [str pair-sep elt-sep]
  (let [pairs (s/split str pair-sep)]
    (into {} (map #(s/split % elt-sep) pairs))))

(defn form-param-extractor [handler]
  (fn [request]
    (let [body (-> request :body slurp)
          params (split-kvps body #"&" #"=")]
      (handler (merge request {:form-params params})))))

(defn content-type [response]
  (-> response :headers (get "Content-Type")))

(defn content-type-middleware [handler type]
  (fn [request]
    (let [basic-response (handler request)
          basic-headers (:headers basic-response)
          new-type (or (content-type basic-response) type)]
      (merge basic-response
             {:headers (merge basic-headers
                              {"Content-Type" new-type})}))))

(defn request-revealer [handler]
  (fn [request]
    (let [basic-response (handler request)
          basic-body (:body basic-response)
          new-body (str basic-body "<pre>" request "</pre>")]
      (if (= "text/html" (content-type basic-response))
        (merge basic-response {:body new-body})
        basic-response))))

(defn default-handler [request]
  {:status 200
   :body (str "You requested " (:uri request))})

(defn wat-handler [request]
  {:status (/ 1 0)})

(defn home-handler [request]
  {:status 200
   :body (v/page v/home)})

(defn login-get-handler [request]
  {:status 200
   :body (v/page v/login)})

(defn user-home-handler [request]
  {:status 200
})

(defn login-post-handler [request]
  (let [username (:username {:form-params request})
        password (:password {:form-params request})]
    (if (auth/get-person {:username username :password password})
      {:status 200
       :body (str "You posted:" (:form-params request)) }
      (login-get-handler request))))

(defn get-extension [file-name]
  (let [dot-index (s/last-index-of file-name ".")]
    (if (> dot-index -1)
      (subs file-name (inc dot-index))
      "")))

(defn static-handler [request]
  (let [static-path (-> request :uri (s/replace #"/static/" ""))
        data (-> static-path j.io/resource slurp)
        extension (get-extension static-path)
        content-type (case extension
                       "css" "text/css"
                       "application/octet-stream")]
    {:status 200
     :headers {"Content-Type" content-type}
     :body data}))

(defn router [request]
  (let [{method :request-method uri :uri} request]
    (if (s/starts-with? uri "/static/")
      static-handler
      (case [method uri]
        [:get "/"] home-handler
        [:get "/wat"] wat-handler
        [:get "/login"] login-get-handler
        [:post "/login"] (fn [request] ((form-param-extractor login-post-handler) request))
        default-handler))))

(defn main-handler
  "Entry point for the ring app"
  [request]
  (let [route [(:request-method request) (:uri request)]
        route-handler (router request)
        handler (-> route-handler
                    (content-type-middleware "text/html")
                    request-revealer
                    error-trap)]
    (handler request)))

