(ns cortez.auth)

(defn in-memory-creds []
  [{:username "Peter"
    :password "hunter2"}
   {:username "Guest"
    :password "Password1"}])

(defn get-person [creds]
  (if-let [matching-creds (some #{creds} (in-memory-creds))]
    matching-creds
    :no-matching-creds))
